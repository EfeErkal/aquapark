using System;
using UnityEngine;

/// <summary>
/// Attach to : Player prefab
/// This component manages all the inputs sent by the player to Movements and every exceptions of being a player
/// </summary>
public class Player : MonoBehaviour
{
    [SerializeField] private float releaseHardness = 1;
    [SerializeField] private float dragControl = 1;
    [SerializeField] private Transform cameraPlaceHolder;
    [SerializeField] private float flyingRotationControl;
    
    private Movements _movements;
    InGameUI _inGameUi;

    private Rigidbody[] _rigidbodies;
    public static int x = 0;

    private void Start()
    {
        _movements = GetComponent<Movements>();
        _inGameUi = FindObjectOfType<InGameUI>();

        _rigidbodies = GetComponentsInChildren<Rigidbody>();
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.transform.CompareTag("Finish")) //Finish is the terrain's tag, when the player flies and falls on the floor
        {
            _movements.enabled = false;
            _inGameUi.End();
        }

        if (other.transform.CompareTag("Collectable"))
        {
            //other.transform.parent = gameObject.transform;
           
            Debug.Log(gameObject.transform.GetChild(x).gameObject.name);
            
            gameObject.transform.GetChild(x).gameObject.SetActive(true);
            x++;
            
            Destroy(other.transform.gameObject);
           
        }

        if (other.transform.CompareTag("Obstacle"))
        {
           // Debug.Break();
           // gameObject.transform.GetChild(x - 1).gameObject.SetActive(false) ;
           // x--;
           // Destroy(other.gameObject);

        }
    }
    
    public void InitializeCamera() //makes the camera follow the character
    {
        //Setting up the camera
        CameraFollow cameraFollow = FindObjectOfType<CameraFollow>();
        cameraFollow.target = transform;
        cameraFollow.placeHolder = cameraPlaceHolder;
    }
    
    void Update()
    {
        
        if (Input.touchCount > 0 || Input.GetMouseButton(0)) //The player drags the character on the sides
        {
            float mouseX = InputManager.MouseX;

            //var mouseX = Input.mousePosition.x;
            if (_movements.onPath && _movements.deviationModifAuthorization)
            {
                //ESK� HAL�
               // _movements.deviation = Mathf.Clamp(_movements.deviation + mouseX * dragControl, -5f, 5f);

                if (mouseX > 0.5f)
                {
                    //5ti
                    _movements.deviation = Mathf.Clamp(_movements.deviation + 20 * dragControl, -10f, 10f);

                }
                else if (mouseX < -0.5f)
                {
                    _movements.deviation = Mathf.Clamp(_movements.deviation - 20 * dragControl, -10f, 10f);
                }
                else if ( mouseX < 0.5f && mouseX > 0)
                {
                    //2 ydi
                    _movements.deviation = Mathf.Clamp(_movements.deviation + 8 * dragControl, -10f, 10f);
                }
                else if (mouseX > -0.5f && mouseX < 0)
                {
                    _movements.deviation = Mathf.Clamp(_movements.deviation - 8 * dragControl, -10f, 10f);
                }


            }
            else if(!_movements.onPath)
            {
                if (mouseX > 0.5f)
                {
                    transform.RotateAround(transform.position, transform.up, flyingRotationControl * 35 * Time.deltaTime);

                }
                else if (mouseX < -0.5f)
                {
                    transform.RotateAround(transform.position, transform.up, flyingRotationControl * -35 * Time.deltaTime);
                }
                else if (mouseX < 0.5f && mouseX > 0)
                {
                    transform.RotateAround(transform.position, transform.up, flyingRotationControl * 20 * Time.deltaTime);
                }
                else if (mouseX > -0.5f && mouseX < 0)
                {
                    transform.RotateAround(transform.position, transform.up, flyingRotationControl * -20 * Time.deltaTime);
                }
                
            }        
        }
        else if(_movements.deviationModifAuthorization) //When the character is released, he returns to the center of the toboggan by itself
            _movements.deviation = Mathf.Lerp(_movements.deviation, 0, Time.deltaTime * releaseHardness);

    }

}